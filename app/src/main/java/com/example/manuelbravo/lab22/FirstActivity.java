package com.example.manuelbravo.lab22;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FirstActivity extends AppCompatActivity {

    ArrayAdapter adapter;
    Context ctx;
    ListView list;
    SharedPreferences preferences;
    FileUpdated update;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        //We start the RssReaderService
        final Intent i = new Intent(this, RssReaderService.class);
        ctx=this.getApplicationContext();
        startService(i);

        //Add the Listview and we set an adapter
        //https://stackoverflow.com/questions/4709870/setonitemclicklistener-on-custom-listview
        list=(ListView) findViewById(R.id.ListView_Elements);
        adapter = new ArrayAdapter<Item>(this,android.R.layout.simple_list_item_1,android.R.id.text1);
        list.setAdapter(adapter);

        //If the user makes click on the list, we show the content of the URL in the second Activity
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Item selectedItem=(Item)list.getItemAtPosition(i);
                String url = selectedItem.getUrl();
                Log.i("getUrl","URL: " + url);
                Intent intent = new Intent(getApplicationContext(),SecondActivity.class);
                intent.putExtra("url", url);
                startActivity(intent);
            }
        });

        //Register the BroadCast Receiver
        //When the RssReadService fetches new data, we will use this receiver to
        //Update the listview.
        update=new FileUpdated();
        registerReceiver(update, new IntentFilter("FileIsUpdated"));


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(update);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_first, menu);
        return true;
    }

    /**
     * If the user clicks on the Settings menu.
     * @param item
     * @return
     */
   @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == R.id.action_settings) {
           changeToSettings();
        }

       return super.onOptionsItemSelected(item);
    }


    /**
     * If the app is resumed
     */
    void OnResume(){
        adapter.clear();
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(this);

    }

    /**
     * Changes to the ThirdActivity. This activity shows the preferences Fragment.
     */
    private void changeToSettings(){
         Intent i = new Intent(this, ThirdActivity.class);
         startActivity(i);

    }


    //https://stackoverflow.com/questions/14376807/how-to-read-write-string-from-a-file-in-android

    /**
     * This method reads from a file the content that is fetched by the RssReadService.
     * And updates the ListView of the FirstActivity.
     * @param view
     */
    public void onUpdate(View view){
        int count=0;
        adapter.clear();

        Item entry;
        String title="";
        String description="";
        String url="";


        int noOfElements =  Integer.parseInt(preferences.getString("key_totalElements", "50"));

        Log.i("noOfElements","No Of Elements:"+noOfElements);
        try {
            InputStream inputStream = ctx.openFileInput("content.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";

              while ( (title = bufferedReader.readLine()) != null && count<noOfElements ) {

                    description=bufferedReader.readLine();
                    url=bufferedReader.readLine();
                    entry =new Item(title,description,url);
                    adapter.add (entry);
                    count++;
                   //Log.i("Adding to Adapter","Item : " + receiveString);

                }

                inputStream.close();

            }
            //inputStream.close();
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }
    }

    /**
     * The user clicks on the Refresh Button
     * The content will be fetched
     * And the listview will be updated
     * @param view
     */
    public void onRefresh(View view) {
        adapter.clear();
        stopService(new Intent(this, RssReaderService.class));
        final Intent i = new Intent(this, RssReaderService.class);
        startService(i);

    }

    /**
     * This Class is a BroadCastReceiver
     * The RssReadService will notify that new content was fetched. Then this class will update
     * the content of the first activity.
     */
    public class FileUpdated extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            onUpdate(list);
        }
    }


}












