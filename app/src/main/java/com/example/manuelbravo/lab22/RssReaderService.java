package com.example.manuelbravo.lab22;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.einmalfel.earl.EarlParser;
import com.einmalfel.earl.Feed;
import com.einmalfel.earl.Item;

import org.w3c.dom.Document;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import static android.content.ContentValues.TAG;

/**
 * Created by manuelbravo on 18.02.2018.
 */

public class RssReaderService extends Service {

    private Context ctx;
    private ListView view;
    SharedPreferences preferences;
    private static Timer timer;


    public RssReaderService (){
        super();
    }

    public RssReaderService(Context c){
        super();
        this.ctx=c;
    }

    /**
     * When the service is called
     *
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.ctx=this.getApplicationContext();
       // new Update().execute();
        startService();
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    /**
     * This method starts the service.
     */
    private void startService(){
        int frequency;

        if(preferences.getString("key_frequency", "600000").equals("")){
            frequency=600000;
         }else{
            frequency=Integer.parseInt(preferences.getString("key_frequency", "600000"));
        }

         Log.i("Frequency", "Frecuency is:"+ frequency);
            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {

                    new Update().execute();
                }
            }, 0,frequency);
    }

    /**
     * Destroys the Service.
     */
    public void onDestroy()
    {
        super.onDestroy();
        timer.cancel();
        // Toast.makeText(this, "Service Stopped ...", Toast.LENGTH_SHORT).show();
    }


    public void setView(ListView viewById) {
        this.view=viewById;
    }


    /**
     * This class will Download the content of a RSSFeeder
     * and save the content in a file.
     *
     */
    class Update extends AsyncTask<Void, Void, Feed> {

        @Override
        protected Feed doInBackground(Void... voids) {
            String[] items;

            try{
                String urlString = preferences.getString("key_url", "https://www.vg.no/rss/feed/?categories=&keywords=&limit=100&format=atom");

                Log.d("Feed", urlString);
                URL url=new URL(urlString);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                InputStream inputStream =connection.getInputStream();
                Feed feed = EarlParser.parseOrThrow(inputStream, 0);

                Log.i(TAG, "Processing feed: " + feed.getTitle());

                try(FileOutputStream openFileOutput=ctx.openFileOutput("content.txt", Context.MODE_PRIVATE );){

                    String title ="";
                    String description="";
                    String link ="";

                    Log.i(TAG, "Item title: " + (title == null ? "N/A" : title));

                    for (Item item : feed.getItems()) {

                        title="Title:"+item.getTitle();
                        description="Description:"+item.getDescription();
                        link =item.getLink();

                        openFileOutput.write(title.getBytes());
                        openFileOutput.write('\n');
                        openFileOutput.write(description.getBytes());
                        openFileOutput.write('\n');
                        openFileOutput.write(link.getBytes());
                        openFileOutput.write('\n');

                    }

                    openFileOutput.close();

                }catch (Exception e){

                }

                return feed;

            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Feed feed) {
            sendFileUpdates();
        }
    }

    /**
     * It will notify to the BroadCast Receiver that the content of
     * the RSSFeeder is fetched.
     */
    private void sendFileUpdates() {
        Intent intent = new Intent("FileIsUpdated");

        sendBroadcast(intent);
    }


}
