package com.example.manuelbravo.lab22;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

/**
 * Created by manuelbravo on 17.02.2018.
 */

public class ThirdActivity extends AppCompatActivity {

    /**
     * Changes the content of the Activity with the content of the Fragment.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new PrefFragment())
                .commit();

    }

}
