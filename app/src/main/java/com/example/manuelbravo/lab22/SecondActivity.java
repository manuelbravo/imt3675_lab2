package com.example.manuelbravo.lab22;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class SecondActivity extends AppCompatActivity {


    private WebView mWebview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
       // super.onCreate(savedInstanceState);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent i = getIntent();
        String url = i.getStringExtra("url");

        mWebview = new WebView(this);
        setContentView(mWebview);
        mWebview.setWebViewClient(new WebViewClient());
        mWebview.loadUrl(url);

      //  Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
       // setSupportActionBar(toolbar);


    }

    /**
     * Dispalys the Settings menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_first, menu);
        return true;
    }


    /**
     * If the Settins menu is clicked.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            changeToSettings();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Change to the ThirdActivity (That will show the Preferences Fragment)
     */
    private void changeToSettings(){
        Intent i = new Intent(this, ThirdActivity.class);
        startActivity(i);

    }


}
