package com.example.manuelbravo.lab22;

/**
 * Created by manuelbravo on 16.03.2018.
 */

/**
 * Adapter class used to save the content (and url) that will be showed in the First Activity.
 */
public class Item {

    String title;
    String description;
    String url;

    public Item(String title, String description, String url){
        this.title=title;
        this.description=description;
        this.url=url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String toString(){
       return this.title+"\n\n"+this.description;
    }


}

